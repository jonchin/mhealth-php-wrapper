<?php
  define('MHEALTH_SERVER', 'https://api-mhealth.att.com');
  define('MHEALTH_AUTH_SERVER', 'http://mhealth.att.com');
  
  
  require 'rest-client-php/rest_client.php';

  class Mhealth{
    protected $accessToken;
    protected $restClient;
    
    public function __construct($accessToken){
      $this->accessToken = $accessToken;
      $this->restClient = new RestClient();
    }
    
    public function GetUserData(){
      if(!$this->accessToken){
        throw Exception('Need access token');
      }
      $userResults = $this->restClient->get($this->MakeURL('/v2/health/user'));
      return $this->ParseResults($userResults);
    }
    
    public function GetDataTypes(){
      $dataTypes = $this->restClient->get($this->MakeURL('/v2/health/unit/types'));
      return $this->ParseResults($dataTypes);
    }
    
    public function GetDataByType($type){
      if(!$this->accessToken){
        throw Exception('Need access token');
      }
      //TODO: add validation on $type
      $dataResults = $this->restClient->get($this->MakeURL('/v2/health/data/' . $type));
      return $this->ParseResults($dataResults);
    }
    
    public function GetDataByMeasureNames($applicationName, $measureNames, $from='', $to=''){
      if(!$this->accessToken){
        throw Exception('Need access token');
      }
      
      //This is super hacky, but there isn't a nicer way to do this in PHP
      $parameters = array();
      if(is_array($measureNames)){
        foreach($measureNames as $measureName){
          if(!isset($parameters['m[]'])){
            $parameters['m[]'] = $applicationName . '/' . $measureName;
          }
          else{
            $parameters['m[]'] = $parameters['m[]'] . '&' . $applicationName . '/' . $measureName;
          }
        }
      }
      if($from != ''){
        $parameters['from'] = $from;
      }
      if($to != ''){
        $parameters['to'] = $to;
      }
      $dataResults = $this->restClient->get($this->MakeURL('/v2/health/data', $parameters));
      return $dataResults;
    }
    
    private function MakeURL($path, $parameters=array()){
      //TODO: check for slashes
      if($this->accessToken){
        $parameters = array_merge($parameters, array('oauth_token' => $this->accessToken));
      }
      return MHEALTH_SERVER . $path . '?' . http_build_str($parameters);
    }
    
    private function ParseResults($results){
      return json_decode($results);
    }
  }
  
	

	



?>

